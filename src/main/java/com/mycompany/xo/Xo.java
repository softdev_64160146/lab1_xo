/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.xo;

import java.util.Scanner;

/**
 *
 * @author 11pro
 */
public class Xo {
    static char[][]   table = {{'-','-','-'},{'-','-','-'},{'-','-','-'},};
    private static char currentPlayer  = 'X';
    private static int row;
    private static int col;
    public static void main(String[] args) {
        printStart();
        while(true) {
            printTable();
            printTurn();
            inputRowCol();

            if (isWinner()) {
                printTable();
                printWinner();
                break;
            }
            if (isDraw()) {
                printTable();
                printDraw();
                break;
            }
            switchPlayer();
        }
       
}

    private static void printStart() {
        System.out.println("Start XO Game!!!");  
    }

    private static void printTable() {
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
}

    private static void printTurn() {
        System.out.println("Turn Player " + currentPlayer);
    }

    private static void inputRowCol() {
        while (true) {
            System.out.print("Please input row col: ");
            Scanner sc = new Scanner(System.in);
            row = sc.nextInt();
            col = sc.nextInt();
            if (table[row - 1][col - 1] == '-') {
                table[row - 1][col - 1] = currentPlayer;
                return;
            }
        }

    }

    private static void switchPlayer() {
        if(currentPlayer=='X'){
            currentPlayer = 'O';
        } else{
            currentPlayer = 'X';
        }
    }

    private static boolean isWinner() {
        if(checkRow()||checkCol()||checkX1()||checkX2()){
          return  true;  
        }
        return  false;
    }

    private static void printWinner() {
        System.out.println("Player " + currentPlayer + " Win!!!");
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int j = 0; j < 3; j++) {
            if (table[j][col - 1]!= currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1() {
         for (int i = 0; i < 3; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;

    }

    private static boolean checkX2() {
       for (int i = 0; i < 3; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    private static void printDraw() {
        System.out.println("Player Draw!");
    }
}
